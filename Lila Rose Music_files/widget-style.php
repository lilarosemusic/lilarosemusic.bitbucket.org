    
    
.widget_doifd_user_reg_form {
width: 300px;
margin-top: 25px;
margin-right: 0px;
margin-bottom: 25px;
margin-left: 0px;
-webkit-border-radius: 4px;
-moz-border-radius: 4px;
border-radius: 4px;
-webkit-box-shadow: inset 0 1px 15px rgba(68,68,68,0.6);
-moz-box-shadow: inset 0 1px 15px rgba(68,68,68,0.6);
box-shadow: inset 0 1px 15px rgba(68,68,68,0.6);
padding: 5px ;
background-color: transparent;
}

.widget_doifd_user_reg_form label {
color: #ffffff;
}

.widget_doifd_user_reg_form ul li {
list-style-type: none;
padding: 0px;
margin: 0px;
}

.widget_doifd_user_reg_form input[type=text] {

width: 180px;
background: transparent;
margin-bottom: 10px;
}    

.doifd_widget_promo_link {
font-size: 0.7em !important;
padding-top: 15px !important;
text-align: center;
}

.doifd_widget_promo_link a:link,
.doifd_widget_promo_link a:visited,
.doifd_widget_promo_link a:hover,
.doifd_widget_promo_link a:active {
color: #CCCCCC !important;
}

.doifd_widget_statusmsg {

margin-bottom: 20px;
width: 100%;
font-size: 1em;
text-align: center;

}

.doifd_widget_waiting {
color: #767676;
text-align: center;
display: none;
font-family : Arial, sans-serif;
font-size:0.8em;
font-weight: bold;
margin: 100px auto;
background: transparent;
}

.widget_h4 {
margin: 5px auto;
text-align: center;
width: 90%;
color: #ffffff;
font-size: 1em;
}

.doifd_privacy_link {
text-align: center;
margin: 10px 0;
font-size: 0.9em;
}