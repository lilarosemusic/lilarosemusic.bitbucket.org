// Call the slider in each gallery
jQuery( document ).ready( function( $ ) {
    $( '.slideshow-container' ).each( function() {
        $( this ).basic_slider({
            animtype    : 'fade',
            height      : 700,
            width       : '100%',
            responsive  : true,
            showmarkers : true,
            automatic   : false,
            animduration: 1000
          });
     });

    // responsive videos
    $('#content').fitVids();

    // Call the function for bg resize
    background_size_calculator();

    // Call on window resize
    $( window ).resize( function() {
        background_size_calculator();
    });

    // Function to change the container size
    function background_size_calculator() {
        divwidth = $( '#content' ).width();
        $( '.awesome-slideshow' ).each( function() {
            if( $(this).parent().parent().is(".entry-content") ) {
                content_width = $( '.entry-content' ).width();
                if( null != content_width ) { divwidth = content_width }
            }
            var ratio = $( this ).attr( 'data-ratio' );
            newheight = divwidth / ratio;
                if( newheight > 800 ) {
                    newheight = 800;
                } else if( newheight < 300 ) {
                    newheight = 300;
                }
                $( this ).height( newheight );
       });
    }
    $( '.main-navigation ul.nav-menu' ).hover(
        function() {
            $( this ).parent().addClass( 'showmenus' );
        },
        function() {
            $( this ).parent().removeClass( 'showmenus' );
        }
    );

    // call menu effect function on load
    menu_effect();

    // call menu effect function on window resize
    $( window ).resize( function() {
        menu_effect();
    });
    // custom scrollbar
    jQuery( '.main-navigation ul' ).mCustomScrollbar({
        scrollInertia: 150,
        advanced: {
            updateOnContentResize: true
        }
    });

    jQuery( 'h1.menu-toggle' ).click( function() {
        jQuery( this ).siblings( '.menu-container' ).slideToggle();
    });


// Add to Lightbox
    if($('.add-to-lightbox').length) {
        $('.add-to-lightbox').live('click',function() {
            if ($(this).hasClass('saved-to-lightbox')) {
                return false;
            } else {
                var id = $(this).attr('id');
                id = id.split('lightbox-');
                $(this).attr('disabled', true);
                $(this).text('Saving...');
                $.ajax({
                    url: sell_media.ajaxurl,
                    type: "POST",
                    dataType: 'json',
                    data: { action : 'awesome_lightbox_ajax', id : id[1] },
                    success:function(data) {
                        if(true==data.success) {
                            $('#lightbox-'+data.postID).text("Saved to lightbox");
                            $('#lightbox-'+data.postID).prev().addClass('lightbox-active');
                            var count = $('.lightbox-menu .lightbox-counter').html();
                            count = parseInt(count) + 1;
                            $('.lightbox-menu .lightbox-counter').html(count);
                            $('#lightbox-'+data.postID).removeAttr("disabled");
                            $('#lightbox-'+data.postID).addClass("saved-to-lightbox");
                        }
                    }
                });
                return false;
            }
        });
    }

    // Remove from Lightbox
    if($('.remove-lightbox').length) {
        $('.remove-lightbox').live('click',function() {
            var id = $(this).attr('id');
            id = id.split('lightbox-');
            $.ajax({
                url: sell_media.ajaxurl,
                type: "POST",
                dataType: 'json',
                data: { action : 'awesome_lightbox_remove_ajax', id : id[1] },
                success:function(data) {
                    if(true==data.success) {
                        $('#lightbox-'+data.postID).parent().remove();
                        var count = $('.lightbox-menu .lightbox-counter').html();
                        count = parseInt(count) - 1;
                        $('.lightbox-menu .lightbox-counter').html(count);
                    }
                }
            });
            return false;
        });
    }

    $('#lightbox .sell-media-grid').hover(function(){
        $(this).find('.remove-lightbox').stop().css({'opacity':'1', 'display':'block'});
    },function(){
        $(this).find('.remove-lightbox').stop().css('opacity','0');
    });

});

// menu effect function
function menu_effect() {
    if ( jQuery( 'body' ).outerWidth() <= 600 ) {
        jQuery( 'body' ).find( '#site-navigation' ).removeClass( 'main-navigation' ).addClass( 'main-small-navigation' ).find( '.menu-container' ).hide();
        jQuery( '#page' ).removeClass( 'large-screen' ).addClass( 'small-screen' );
    } else {
        jQuery( 'body' ).find( '#site-navigation' ).removeClass( 'main-small-navigation' ).addClass( 'main-navigation' ).find( '.menu-container' ).show();
        jQuery( '#page' ).removeClass( 'small-screen' ).addClass( 'large-screen' );
    }
}
